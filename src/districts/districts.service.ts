import { Injectable } from '@nestjs/common';
import { CreateDistrictDto } from './dto/create-district.dto';
import { UpdateDistrictDto } from './dto/update-district.dto';
import { InjectModel } from '@nestjs/sequelize';
import { Region } from 'src/regions/models/region.model';
import { District } from './models/district.model';

@Injectable()
export class DistrictsService {
 constructor (@InjectModel(District) private  districtRepo: typeof District){}
  create(createDistrictDto: CreateDistrictDto) {
    this.districtRepo.create(createDistrictDto)
    return 'This action adds a new district';
  }

  findAll() {
    return `This action returns all districts`;
  }
  async findAllDistrictByRegionID(region_id: number){
    const districts = await this.districtRepo.findAll({
      where: { region_id: region_id } // postgres query: SELECT * FROM districts WHERE region_id;
  });

  return districts;
  }
  findOne(id: number) {
    return `This action returns a #${id} district`;
  }

  update(id: number, updateDistrictDto: UpdateDistrictDto) {
    return `This action updates a #${id} district`;
  }

  remove(id: number) {
    return `This action removes a #${id} district`;
  }
}
