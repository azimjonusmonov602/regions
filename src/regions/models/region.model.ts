import { Column, DataType, Model, Table } from "sequelize-typescript";

@Table({tableName: "regions"})
export class Region extends Model<Region>{
    @Column({
        type: DataType.SMALLINT,
        autoIncrement: true,
        primaryKey: true
    })
    id: number;

    @Column({
        type: DataType.STRING
    })
    name: string;
}
