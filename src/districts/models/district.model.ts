import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from "sequelize-typescript";
import { Region } from "src/regions/models/region.model";

@Table({tableName: "district"})
export class District extends Model<District> {
    @Column({
        type: DataType.INTEGER,
        autoIncrement: true,
        primaryKey: true
    })
    id: number;

    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    name: string;

    @Column({
        type: DataType.SMALLINT,
        allowNull: false
    })
    @ForeignKey(() => Region) 
    region_id: number;

    @BelongsTo(() => Region) 
    region: Region;
}
